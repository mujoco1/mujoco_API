#!/usr/bin/env python
# demonstration of markers (visual-only geoms)

import numpy as np
from mujoco_py import load_model_from_path, MjSim, MjViewer
from mujoco_py.generated.const import GEOM_SPHERE
model = load_model_from_path("./ant_model.xml")
sim = MjSim(model)
viewer = MjViewer(sim)
step = 0

while True:
    viewer.add_marker(type=GEOM_SPHERE,
                                   pos=np.asarray([3, 1, 0.5]),
                                   rgba=np.asarray([0.7, 0.13, 0.17, 1.0]),
                                   size=np.asarray([0.6] * 3))
    viewer.render()