#!/usr/bin/env python

import math
import os
import numpy as np
from mujoco_py import load_model_from_xml, MjSim, MjViewer, load_model_from_path
import time

# The script is copied from the some parts of the  mujoco-py/mujoco_py/tests/test_cymj.py
#https://github.com/openai/mujoco-py/blob/cd247d5fae39f21e913104350e26ac7254fa4b93/mujoco_py/tests/test_cymj.py#L442

MODEL_XML = """
<mujoco>
        <worldbody>
            <body name="body1" pos="0 0 0">
                <joint name="a" axis="1 0 0" pos="0 0 0" type="slide"/>
                <geom name="geom1" pos="0 0 0" size="1.0"/>
                <body name="body2" pos="0 0 1">
                    <joint name="b" axis="1 0 0" pos="0 0 1" type="slide"/>
                    <geom name="geom2" pos="0 0 0" size="0.5"/>
                    <site name="site1" size="0.1"/>
                </body>
            </body>
        </worldbody>
        <actuator>
            <motor joint="a"/>
            <motor joint="b"/>
        </actuator>
    </mujoco>
"""




model = load_model_from_xml(MODEL_XML)
sim = MjSim(model)
viewer = MjViewer(sim)
sim.reset()
sim.forward()
viewer.render()

time.sleep(3)
# Check that xvelp starts out at zero (since qvel is zero)
site1_xvelp = sim.data.get_site_xvelp('site1')
np.testing.assert_allclose(site1_xvelp, np.zeros(3))
# Push the base body and step forward to get it moving
sim.data.ctrl[0] = 1e9
sim.step()
sim.forward()
# Check that the first body has nonzero xvelp
body1_xvelp = sim.data.get_body_xvelp('body1')
assert not np.allclose(body1_xvelp, np.zeros(3))
# Check that the second body has zero xvelp (still)
body2_xvelp = sim.data.get_body_xvelp('body2')
np.testing.assert_allclose(body2_xvelp, np.zeros(3))
# Check that this matches the batch (gathered) getter property
np.testing.assert_allclose(body2_xvelp, sim.data.body_xvelp[2])

print(body1_xvelp)

while True:
    viewer.render()