#!/usr/bin/env python

import math
import os
import numpy as np
from mujoco_py import load_model_from_xml, MjSim, MjViewer, load_model_from_path

model = load_model_from_path("assets/fetch/push.xml")
sim = MjSim(model)
viewer = MjViewer(sim)
step = 0
viewer.render()
while True:
    sim.step()
    viewer.render()
